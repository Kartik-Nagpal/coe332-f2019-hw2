from flask import Flask, jsonify, request, abort
import json

# The main Flask app
app = Flask(__name__)

# Data from a json file
data = json.load(open('./coe332.json', 'r'))

@app.route('/')
def coe332():
    return jsonify(data)


@app.route('/instructors')
def getInstructs():
    return jsonify(data["instructors"])


@app.route('/instructors/<int:id>')
def getInstructsByID(id):
    try:
        return jsonify(data["instructors"][id])
    except:
        abort(404)


@app.route('/instructors/<int:id>/<string:choice>')
def getInstructsData(id, choice):
    try:
        return jsonify(data["instructors"][id][choice])
    except:
        abort(404)


@app.route('/meeting')
def getMeets():
    return jsonify(data["meeting"])


@app.route('/meeting/<string:choice>')
def getMeetsByID(choice):
    try:
        return jsonify(data["meeting"][choice])
    except:
        abort(404)


@app.route('/assignments', methods=['GET'])
def getAssigns():
    return jsonify(data["assignments"])
    

@app.route('/assignments/<int:id>', methods=['GET'])
def getAssignsByID(id):
    try:
        return jsonify(data["assignments"][id])
    except:
        abort(404)


@app.route('/assignments/<int:id>/<string:choice>', methods=['GET'])
def getAssignsData(id, choice):
    try:
        return jsonify(data["assignments"][id][choice])
    except:
        abort(404)


@app.route('/assignments', methods=['POST'])
def postAssign():
    requestDict = json.loads(request.data.decode('utf-8'))
    data['assignments'].append(requestDict)
    return getAssigns()